## Install foundation on Rails 6 App.

### Add JQuery

```bash
yarn add jquery
```

### Add new plugin to webpacker in `config/webpack/environment.js`

```js
const { environment } = require('@rails/webpacker')
const webpack = require("webpack")

environment.plugins.append("Provide", new webpack.ProvidePlugin({
  $: 'jquery',
  jQuery: 'jquery'
}))

module.exports = environment
```
### Include jQuery in `app/javascript/packs/application.js`

```js
require("@rails/ujs").start()
require("turbolinks").start()
require("jquery")
require("@rails/activestorage").start()
require("channels")

// test code for jQuery
$(document).on('turbolinks:load', function() {
  console.log('hello jQuery')
})
```

### test jQuery

```bash
rails g controller home index
```

`config/routes.rb`

```ruby
Rails.application.routes.draw do
  get 'home/index'

  root 'home#index'
end
```

after the test `do not forget` the remove the following code

`app/javascript/packs/application.js`

```js
// test code for jQuery
$(document).on('turbolinks:load', function() {
  console.log('hello jQuery')
})
```

### Add foundation

```bash
yarn add foundation-sites motion-ui
```

```bash
mkdir app/javascript/src
```

`app/javascript/src/application.scss`

```css
@import '~foundation-sites/scss/foundation';
@include foundation-everything;
@import '~motion-ui/motion-ui';
```

`app/javascript/packs/application.js`

```js
require("@rails/ujs").start()
require("turbolinks").start()
require("jquery")
require("@rails/activestorage").start()
require("channels")

import "foundation-sites"
require("src/application")

$(document).on('turbolinks:load', function() {
  $(document).foundation()
});
```

`app/views/layouts/application.html.erb`

```ruby
<%= stylesheet_pack_tag 'application', media: 'all', 'data-turbolinks-track': 'reload' %>
```
