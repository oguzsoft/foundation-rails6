require("@rails/ujs").start()
require("turbolinks").start()
require("jquery")
require("@rails/activestorage").start()
require("channels")

import "foundation-sites"
require("src/application")

$(document).on('turbolinks:load', function() {
  $(document).foundation()
});